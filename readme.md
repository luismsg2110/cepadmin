#Sistema Auth + CRUD + Web Service con Laravel

## Auth Laravel
Sistema de autenticación proporcionado por laravel, para probar el sistema utilice:
email: admin@admin.com	
password: admin123

## CRUD Laravel
En este sistema se logra ver, guadar, editar y eliminar información en la base de datos.
Información como textos, fechas e imagenes.
Se guarda informacion de cuatro items: Noticias, Eventos, Conferencias y Serie Actual.
Cada una se guarda en una tabla diferente.

##Web Service
Cada una de esta información almacenada en BD (MySql) puede ser accedida a través de servicio web:

1) Noticias (Muestra la información con la opción "Mostrar" activada)
https://luismsg2110.000webhostapp.com/blog/public/api/noticias

2) Eventos (Muestra la información con la opción "Mostrar" activada)
https://luismsg2110.000webhostapp.com/blog/public/api/actividades

3) Conferencias (Muestra todos los registros)
https://luismsg2110.000webhostapp.com/blog/public/api/conferencias

4) Serie (Muestra el último registro con la opción "Mostrar" activada)
https://luismsg2110.000webhostapp.com/blog/public/api/serie

------------------------------------------------------------------------------------------------------------------

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeriescurrentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seriescurrent', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status');
            $table->date('date');
            $table->string('title');
            $table->string('review');
            $table->string('content');
            $table->string('pastor');
            $table->string('pathimage');
            $table->string('link');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seriescurrent');
    }
}

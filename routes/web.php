<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('noticia', 'NewsController');
Route::resource('serie', 'SeriesController');
Route::resource('conferencias', 'ConferencesController');
Route::resource('actividades', 'ActivitiesController');

Route::get('api/noticias','ApiCepController@getNews');
Route::get('api/serie','ApiCepController@getSerie');
Route::get('api/conferencias','ApiCepController@getConferencias');
Route::get('api/actividades','ApiCepController@getActividades');

Route::post('api/send-mail','MailController@sendMail');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

@extends('layouts.app')

@section('content')
<div class="row">
  <section class="content" style="width: 100%">
    <div class="">
      <div class="panel panel-default">
        <div class="container-fluid row mx-auto">
          <div class="mr-auto" ><h3>Noticias</h3></div>
          <div class="ml-auto">
            <div class="btn-group">
              <a href="{{ route('noticia.create') }}" class="btn btn-info" >Añadir Noticia
                <i class="fas fa-plus"></i>
              </a>
            </div>
          </div>
        </div>
          <div class="table mt-2" >
            <table class="table table-striped">
             <thead>
               <th>#</th>
               <th>Fecha</th>
               <th>Título</th>
               <th>Ministerio</th>
               <th>Contenido</th>
               <th>Portada</th>
               <th>Mostrar</th>
               <th>Editar</th>
               <th>Eliminar</th>
             </thead>
             <tbody>
              @if($noticia->count())  
              @foreach($noticia as $noti)  
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$noti->date}}</td>
                <td>{{$noti->title}}</td>
                <td>{{$noti->ministerios}}</td>
                <td>{{$noti->content}}</td>
                <td>
                  <img src="{{'../../../assets/img/'.$noti->pathimage}}" style="height: 200px; width: 200px">
                </td>
                <td>
                  @if($noti->status)  
                  <input checked="checked" class="form-check-input mx-auto" type="checkbox" id="defaultCheck1" disabled>
                  @else
                  <input class="form-check-input mx-auto" type="checkbox" value="" id="defaultCheck1" disabled>
                  @endif
                </td>
                <td><a class="btn btn-primary btn-xs" href="{{action('NewsController@edit', $noti->id)}}" ><span class="fas fa-edit"></span></a></td>
                <td>
                  <form action="{{ action('NewsController@destroy', $noti->id) }}" method="post">
                   @csrf
                   @method('DELETE')
                   <button class="btn btn-danger btn-xs" type="submit"><span class="fas fa-trash-alt"></span></button>
                 </form>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="9">No hay registros !!</td>
              </tr>
              @endif
            </tbody>
 
          </table>
        </div>
      
      
    </div>
  </div>
</section>
 
@endsection
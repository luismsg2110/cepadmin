@extends('layouts.app')
@section('content')
<div class="row">
		<div class="mx-auto" style="width: 80%">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Editar Noticia</h3>
				</div>
				<div class="panel-body">					
					<div class="table-container">
						<form method="POST" action="{{route('serie.update', $serie->id)}}"  role="form"  enctype="multipart/form-data">
							{{ csrf_field() }}
							<input name="_method" type="hidden" value="PATCH">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" name="title" id="title" class="form-control input-sm" value="{{$serie->title}}" required>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" name="review" id="review" class="form-control input-sm" value="{{$serie->review}}" required>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" name="pastor" id="pastor" class="form-control input-sm" value="{{$serie->pastor}}" required>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" name="link" id="link" class="form-control input-sm" value="{{$serie->link}}" required>
									</div>
								</div>
							</div>
 
							<div class="form-group">
								<textarea name="content" class="form-control input-sm">{{$serie->content}}</textarea>
							</div>
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group row">
									  <label for="example-datetime-local-input" class="col-2 col-form-label">Fecha</label>
									  <div class="col-10">
									    <input class="form-control" type="date" value="{{$serie->date}}" name="date" id="example-datetime-local-input" required>
									  </div>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-check">
									  <label class="form-check-label">
									  	@if($serie->status)
									    <input checked="checked" class="form-check-input" type="checkbox" value="" name="status">
									    @else
									    <input class="form-check-input" type="checkbox" value="1" name="status">
									    @endif
									    Mostrar serie
									  </label>
									</div>
								</div>
							</div>
							<div class="form-group row">
					          <label class="col-md-3 control-label">Subir imagen</label>
					          <div class="col-md-9">
					            <input type="file" class="form-control" name="pathimage">
					          </div>
					        </div>
					        
							<div class="row">
									<input type="submit"  value="Actualizar" class="btn btn-success btn-block col-md-5 mx-auto">
									<a href="{{ route('serie.index') }}" class="btn btn-info btn-block col-md-5 mx-auto" style=" margin: 0px 0px 0px 20px">Atrás</a> 
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
	@endsection
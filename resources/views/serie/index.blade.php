@extends('layouts.app')
@section('content')
<div class="row">
  <section class="content" style="width: 100%">
    <div class="">
      <div class="panel panel-default">
        <div class="container-fluid row mx-auto">
          <div class="mr-auto" ><h3>Serie Actual</h3></div>
          <div class="ml-auto">
            <div class="btn-group">
              <a href="{{ route('serie.create') }}" class="btn btn-info" >Añadir Serie
                <i class="fas fa-plus"></i>
              </a>
            </div>
          </div>
        </div>
          <div class="table mt-2">
            <table class="table table-striped">
             <thead>
               <th>#</th>
               <th>Título</th>
               <th>Contenido</th>
               <th>Pastor</th>
               <th>Video</th>
               <th>Portada</th>
               <th>Mostrar</th>
               <th>Editar</th>
               <th>Eliminar</th>
             </thead>
             <tbody>
              @if($series->count())  
              @foreach($series as $serie)  
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$serie->title}}</td>
                <td>{{$serie->content}}</td>
                <td>{{$serie->pastor}}</td>
                <td><a href="{{$serie->link}}" target="_blank">Youtube</a> </td>
                <td>
                  <img src="{{'../../../assets/img/'.$serie->pathimage}}" style="height: 200px; width: 200px">
                </td>
                <td>
                  @if($serie->status)  
                  <input checked="checked" class="form-check-input mx-auto" type="checkbox" id="defaultCheck1" disabled>
                  @else
                  <input class="form-check-input mx-auto" type="checkbox" value="" id="defaultCheck1" disabled>
                  @endif
                </td>
                <td><a class="btn btn-primary btn-xs" href="{{action('SeriesController@edit', $serie->id)}}" ><span class="fas fa-edit"></span></a></td>
                <td>
                  <form action="{{ action('SeriesController@destroy', $serie->id) }}" method="post">
                   @csrf
                   @method('DELETE')
                   <button class="btn btn-danger btn-xs" type="submit"><span class="fas fa-trash-alt"></span></button>
                 </form>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="9">No hay registros !!</td>
              </tr>
              @endif
            </tbody>
 
          </table>
        </div>
      
      
    </div>
  </div>
</section>
 
@endsection
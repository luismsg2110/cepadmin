@extends('layouts.app')
@section('content')
<div class="row">

		<div class="mx-auto" style="width: 80%">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif
 
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Nueva Conferencia</h3>
				</div>
				<div class="panel-body">					
					<div class="table-container">
						<form method="POST" action="{{ route('conferencias.store') }}"  role="form" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" name="title" id="title" class="form-control input-sm" placeholder="Título" required>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" name="pastor" id="pastor" class="form-control input-sm" placeholder="Nombre de Pastor"
										required>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="form-group">
									<input type="text" name="linkyoutube" id="linkyoutube" class="form-control input-sm" placeholder="Link de Youtube" required>
								</div>
							</div>
							
							<div class="form-group row">
					          <label class="col-md-2 control-label">Subir imagen</label>
					          <div class="col-md-10">
					            <input type="file" class="form-control" name="pathimage" required>
					          </div>
					        </div>
					        
							<div class="row">
								<input type="submit"  value="Guardar" class="btn btn-success btn-block col-md-5 mx-auto">
								<a href="{{ route('conferencias.index') }}" class="btn btn-info btn-block col-md-5 mx-auto" style=" margin: 0px 0px 0px 20px">Atrás</a> 
							</div>
						</form>
					</div>
				</div>
 
			</div>
		</div>
	
	@endsection
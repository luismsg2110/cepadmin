@extends('layouts.app')
@section('content')
<div class="row">
  <section class="content" style="width: 100%">
    <div class="">
      <div class="panel panel-default">
        <div class="container-fluid row mx-auto">
          <div class="mr-auto" ><h3>Prédicas - Conferecias</h3></div>
          <div class="ml-auto">
            <div class="btn-group">
              <a href="{{ route('conferencias.create') }}" class="btn btn-info" >Añadir Conferencia
                <i class="fas fa-plus"></i>
              </a>
            </div>
          </div>
        </div>
          <div class="table mt-2">
            <table class="table table-striped">
             <thead>
               <th>#</th>
               <th>Título</th>
               <th>Pastor</th>
               <th>Video</th>
               <th>Portada</th>
               <th>Editar</th>
               <th>Eliminar</th>
             </thead>
             <tbody>
              @if($conferencias->count())  
              @foreach($conferencias as $conferencia)  
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$conferencia->title}}</td>
                <td>{{$conferencia->pastor}}</td>
                <td>
                  <a href="{{$conferencia->linkyoutube}}" target="_blanck">Youtube</a>
                </td>
                <td>
                  <img src="{{'../../../assets/img/'.$conferencia->pathimage}}" style="height: 200px; width: 200px">
                </td>
                <td>
                  <a class="btn btn-primary btn-xs" href="{{action('ConferencesController@edit', $conferencia->id)}}" ><span class="fas fa-edit"></span></a>
                </td>
                <td>
                  <form action="{{ action('ConferencesController@destroy', $conferencia->id) }}" method="post">
                   @csrf
                   @method('DELETE')
                   <button class="btn btn-danger btn-xs" type="submit"><span class="fas fa-trash-alt"></span></button>
                 </form>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
            </tbody>
 
          </table>
        </div>
      
      
    </div>
  </div>
</section>
 
@endsection
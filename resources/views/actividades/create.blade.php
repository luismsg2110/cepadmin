@extends('layouts.app')
@section('content')
<div class="row">

		<div class="mx-auto" style="width: 80%">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif
 
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Nueva Actividad</h3>
				</div>
				<div class="panel-body">					
					<div class="table-container">
						<form method="POST" action="{{ route('actividades.store') }}"  role="form" enctype="multipart/form-data">
							{{ csrf_field() }}
 
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group row">
									  <label for="example-datetime-local-input" class="col-2 col-form-label">Fecha</label>
									  <div class="col-10">
									    <input class="form-control" type="date" value="" name="date" id="example-datetime-local-input" required>
									  </div>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-check">
									  <label class="form-check-label">
									    <input class="form-check-input" type="checkbox" value="1" name="status">
									    Mostrar noticia
									  </label>
									</div>
								</div>
							</div>
							<div class="form-group row">
					          <label class="col-md-3 control-label">Subir imagen</label>
					          <div class="col-md-9">
					            <input type="file" class="form-control" name="pathimage" required>
					          </div>
					        </div>
					        
							<div class="row">
									<input type="submit"  value="Guardar" class="btn btn-success btn-block col-md-5 mx-auto">
									<a href="{{ route('actividades.index') }}" class="btn btn-info btn-block col-md-5 mx-auto" style=" margin: 0px 0px 0px 20px">Atrás</a> 
							</div>
						</form>
					</div>
				</div>
 
			</div>
		</div>
	
	@endsection
@extends('layouts.app')
@section('content')
<div class="row">
  <section class="content" style="width: 100%">
    <div class="">
      <div class="panel panel-default">
        <div class="container-fluid row mx-auto">
          <div class="mr-auto" ><h3>Actividades</h3></div>
          <div class="ml-auto">
            <div class="btn-group">
              <a href="{{ route('actividades.create') }}" class="btn btn-info" >Añadir Actividad
                <i class="fas fa-plus"></i>
              </a>
            </div>
          </div>
        </div>
          <div class="table mt-2">
            <table class="table table-striped">
             <thead>
               <th>#</th>
               <th>Fecha</th>
               <th>Mostrar</th>
               <th>Imagen</th>
               <th>Editar</th>
               <th>Eliminar</th>
             </thead>
             <tbody>
              @if($actividades->count())  
              @foreach($actividades as $actividad)  
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$actividad->date}}</td>
                <td>
                  @if($actividad->status)  
                  <input checked="checked" class="form-check-input mx-auto" type="checkbox" id="defaultCheck1" disabled>
                  @else
                  <input class="form-check-input mx-auto" type="checkbox" value="" id="defaultCheck1" disabled>
                  @endif
                </td>
                <td>
                  <img src="{{'../../../assets/img/'.$actividad->pathimage}}" style="height: 200px; width: 200px">
                </td>
                <td>
                  <a class="btn btn-primary btn-xs" href="{{action('ActivitiesController@edit', $actividad->id)}}" ><span class="fas fa-edit"></span></a>
                </td>
                <td>
                  <form action="{{ action('ActivitiesController@destroy', $actividad->id) }}" method="post">
                   @csrf
                   @method('DELETE')
                   <button class="btn btn-danger btn-xs" type="submit"><span class="fas fa-trash-alt"></span></button>
                 </form>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
            </tbody>
 
          </table>
        </div>
      
      
    </div>
  </div>
</section>
 
@endsection
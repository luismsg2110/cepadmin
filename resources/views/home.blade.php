@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <img class="card-img-top" src="../../../assets/img/admin.jpg" alt="image admin">
                    
                <div class="card-body">
                    <h5 class="card-title">Gestión de Página Web</h5>
                    <p class="card-text">Aquí podrá administar información que se verá en la web www.cepeniel.org.</p>
                </div>

                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <a href="{{route('noticia.index')}}">Administar noticias del CEP</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{route('actividades.index')}}">Administar eventos de a iglesia</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{route('conferencias.index')}}">Administar Conferencias o Prédicas</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{route('serie.index')}}">Administar serie actual</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

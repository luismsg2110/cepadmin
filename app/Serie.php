<?php

namespace App;

use App\Serie;
use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{
    //
    protected $table = 'seriescurrent';
    protected $fillable = ['status','date','title','review','content','pastor','pathimage','link'

	];
	protected $attributes = [
		'status' => 0
	];
}

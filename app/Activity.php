<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    //
    protected $table = 'activities';
    protected $fillable = ['status','date','pathimage'
	];
	protected $attributes = [
		'status' => 0
	];
}

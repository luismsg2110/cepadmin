<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    //
    protected $table = 'news';
    protected $fillable = ['status','date','ministerios','title','content','pathimage',
	];
	protected $attributes = [
		'status' => 0
	];
}

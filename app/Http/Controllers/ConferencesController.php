<?php

namespace App\Http\Controllers;

use App\Conference;
use Illuminate\Http\Request;

class ConferencesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $conferencias = Conference::all(); 
        return view('conferencias.index',compact('conferencias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('conferencias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $conferencia = $request->all();
        $this->validate($request, [
            'title' => 'required',
            'pastor' => 'required',
            'linkyoutube' => 'required',
            'pathimage' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        $name= $request->file('pathimage')->getClientOriginalName();
        $destination = base_path().'/public/assets/img/';
        $request->file('pathimage')->move($destination, $name);
        $conferencia['pathimage'] = $name;
        Conference::create($conferencia);
        return redirect()->route('conferencias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $conferencia=Conference::find($id);
        return view('conferencias.edit',compact('conferencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $conferencia = $request->all();
        $this->validate($request, [
            'title' => 'required',
            'pastor' => 'required',
            'linkyoutube' => 'required',
            'pathimage' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);
        if($request->hasfile('pathimage')) 
        { 
            $name= $request->file('pathimage')->getClientOriginalName();
            $destination = base_path().'/public/assets/img/';
            $request->file('pathimage')->move($destination, $name);
            $conferencia['pathimage'] = $name;
        }else{
            $imgcurrent = Conference::find($id);
            $conferencia['pathimage'] = $imgcurrent['pathimage'];
        }

        $conferencia['status'] = $request->status ? 1 : 0;
        
        Conference::find($id)->update($conferencia);
        return redirect()->route('conferencias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Conference::find($id)->delete();
        return redirect()->route('conferencias.index');
    }
}

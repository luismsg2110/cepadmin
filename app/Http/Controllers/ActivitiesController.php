<?php

namespace App\Http\Controllers;

use App\Activity;
use Illuminate\Http\Request;

class ActivitiesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $actividades = Activity::all();
        return view('actividades.index',compact('actividades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('actividades.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $actividad = $request->all();
        $this->validate($request, [
            'date' => 'required',
            'pathimage' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        $name= $request->file('pathimage')->getClientOriginalName();
        $destination = base_path().'/public/assets/img/';
        $request->file('pathimage')->move($destination, $name);
        $actividad['pathimage'] = $name;
        Activity::create($actividad);
        return redirect()->route('actividades.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $actividad=Activity::find($id);
        return view('actividades.edit',compact('actividad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $actividad = $request->all();
        $this->validate($request, [
            'date' => 'required',
            'pathimage' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);
        if($request->hasfile('pathimage')) 
        { 
            $name= $request->file('pathimage')->getClientOriginalName();
            $destination = base_path().'/public/assets/img/';
            $request->file('pathimage')->move($destination, $name);
            $actividad['pathimage'] = $name;
        }else{
            $imgcurrent = Activity::find($id);
            $actividad['pathimage'] = $imgcurrent['pathimage'];
        }

        $actividad['status'] = $request->status ? 1 : 0;
        
        Activity::find($id)->update($actividad);
        return redirect()->route('actividades.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Activity::find($id)->delete();
        return redirect()->route('actividades.index');
    }
}

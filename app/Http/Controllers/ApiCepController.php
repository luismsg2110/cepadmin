<?php

namespace App\Http\Controllers;

use App\Noticia;
use App\Serie;
use App\Conference;
use App\Activity;
use Illuminate\Http\Request;

class ApiCepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNews()
    {
        //
        $noticias = Noticia::all()->where('status', 1);
        return response()->json($noticias); 
    }
    
    public function getSerie()
    {
        //
        $serie = Serie::where('status', 1)
        ->orderBy('id', 'desc')
        ->first();
        return response()->json($serie); 
    }

    public function getConferencias()
    {
        //
        $conferencias = Conference::all();
        return response()->json($conferencias); 
    }

    public function getActividades()
    {
        //
        $actividades = Activity::all()->where('status', 1);
        return response()->json($actividades); 
    }
}

<?php

namespace App\Http\Controllers;

use App\Serie;
use Illuminate\Http\Request;

class SeriesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $series = Serie::all(); 
        return view('serie.index',compact('series'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('serie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $serie = $request->all();
        $this->validate($request, [
            'title' => 'required',
            'review' => 'required',
            'link' => 'required',
            'pathimage' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'content' => 'required',
            'date' => 'required',
        ]);
        $name= $request->file('pathimage')->getClientOriginalName();
        $destination = base_path().'/public/assets/img/';
        $request->file('pathimage')->move($destination, $name);
        $serie['pathimage'] = $name;
        Serie::create($serie);
        return redirect()->route('serie.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $serie=Serie::find($id);
        return view('serie.edit',compact('serie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $serie = $request->all();
        $this->validate($request, [
            'title' => 'required',
            'review' => 'required',
            'link' => 'required',
            'pathimage' => 'image|mimes:jpeg,png,jpg|max:2048',
            'content' => 'required',
            'date' => 'required',
        ]);
        if($request->hasfile('pathimage')) 
        { 
            $name= $request->file('pathimage')->getClientOriginalName();
            $destination = base_path().'/public/assets/img/';
            $request->file('pathimage')->move($destination, $name);
            $serie['pathimage'] = $name;
        }else{
            $imgcurrent = Serie::find($id);
            $serie['pathimage'] = $imgcurrent['pathimage'];
        }

        $serie['status'] = $request->status ? 1 : 0;
        
        Serie::find($id)->update($serie);
        return redirect()->route('serie.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Serie::find($id)->delete();
        return redirect()->route('serie.index');
    }
}

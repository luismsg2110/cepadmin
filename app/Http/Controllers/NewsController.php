<?php

namespace App\Http\Controllers;

use App\Noticia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $noticia = Noticia::all();
        return view('peniel.index',compact('noticia'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('peniel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $noticia = $request->all();
        $this->validate($request, [
            'title' => 'required',
            'ministerios' => 'required',
            'pathimage' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'content' => 'required',
            'date' => 'required',
        ]);
        $name= $request->file('pathimage')->getClientOriginalName();
        $destination = base_path().'/public/assets/img/';
        $request->file('pathimage')->move($destination, $name);
        $noticia['pathimage'] = $name;
        Noticia::create($noticia);
        return redirect()->route('noticia.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $noticia=Noticia::find($id);
        return view('peniel.edit',compact('noticia'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $noticia = $request->all();
        $this->validate($request, [
            'title' => 'required',
            'ministerios' => 'required',
            'content' => 'required',
            'pathimage' => 'image|mimes:jpeg,png,jpg|max:2048',
            'date' => 'required',
        ]);
        if($request->hasfile('pathimage')) 
        { 
            $name= $request->file('pathimage')->getClientOriginalName();
            $destination = base_path().'/public/assets/img/';
            $request->file('pathimage')->move($destination, $name);
            $noticia['pathimage'] = $name;
        }else{
            $imgcurrent = Noticia::find($id);
            $noticia['pathimage'] = $imgcurrent['pathimage'];
        }

        $noticia['status'] = $request->status ? 1 : 0;
        
        Noticia::find($id)->update($noticia );
        return redirect()->route('noticia.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Noticia::find($id)->delete();
        return redirect()->route('noticia.index');

    }
}

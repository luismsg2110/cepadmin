<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    /**
     * Send Email method.
     * *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendMail(Request $request)
    {
        $data = $request->all();
        try{$validator = $request->validate([
                'asunto' => 'required|max:255',
                'nombre' => 'required|max:255',
                'telefono' => 'required|max:255',
                'mensaje' => 'required'
            ]);}
        catch(\Exception $e){ return \response($e->errors(),400);}
        Mail::to("luismsg2110@gmail.com")->send(new ContactMail($data));
        
    }
}
